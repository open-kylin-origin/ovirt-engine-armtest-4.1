#!/bin/sh
#
# engine-autobackup.sh - oVirt engine autobackup utility
# Copyright (C) 2015 CS2C.
#
#

SYSTEM_MANAGER=
ACTION=
LOG=/var/log/ovirt-engine/engine-autobackup.log
CRONFILE=/etc/cron.daily/engine-autobackup
ENGINE_FILE1=/etc/ovirt-engine-setup.conf.d/20-setup-ovirt-post.conf
ENGINE_FILE2=/etc/ovirt-engine/engine.conf.d/10-setup-database.conf
CRONDINIT=/etc/init.d/crond
SOURCE_CRONFILE=/usr/share/ovirt-engine/bin/engine-autobackup

usage() {
    cat << __EOF__
engine-autobackup: enable or disable autobackup function of ovirt-engine environment
USAGE:
    $0 <autobackup action>
 autobackup action is one of the following:
    on                          enable autobackup function
    off                         disable autobackup function
    status                      check the status of atuobackup function
__EOF__
    return 0
}

#parse the arguments from cmdline
parseArgs() {
    if [ $# -ne 1 ]; then
        echo "The number of argument should be 1"
        usage
        exit 0
    fi
    if [ -n "$1" ]; then
        if [ "$1" = "on" ]; then
            ACTION="enable"
        elif [ "$1" = "off" ]; then
            ACTION="disable"
        elif [ "$1" = "status" ]; then
            ACTION="status"
        else
            usage
            exit 0
        fi
    else
        usage
        exit 0
    fi
}

# Check which system service manager is used
checksystem() {
    SYSTEMD_PID=`pidof systemd`
    INIT_PID=`pidof init`
    if [ "$SYSTEMD_PID" = "1" ]; then
        SYSTEM_MANAGER="SYSTEMD"
    elif [ "$INIT_PID" = "1" ]; then
        SYSTEM_MANAGER="INIT"
    fi
}

dostatus() {
    log "Get the status of autobackup function."
    if [ -f "$CRONFILE" ]; then
        # for systemv
        if [ -f "$CRONDINIT" ]; then
            $CRONDINIT status
            if [ $? -eq 0 ]; then
                 output "Status of autobackup function : Enabled"
                 output "The backup action will be executed automatically everyday."
            else
                 output "Status of autobackup function : Disabled"
                 output "The crond service is not running."
            fi
        # for systemd
        elif [ "$SYSTEM_MANAGER" = "SYSTEMD" ]; then
            systemctl status crond
            if [ $? -eq 0 ]; then
                 output "Status of autobackup function : Enabled"
                 output "The backup action will be executed automatically everyday."
            else
                 output "Status of autobackup function : Disabled"
                 output "The crond service is not running."
            fi
        else
            output "Status of autobackup function : Disabled"
            output "Cannot find crond service."
        fi
    else
        output "Status of autobackup function : Disabled"
    fi
}

doenable() {
    log "Enable autobackup function."

    # Check the crond file
    if [ ! -f "$CRONFILE" ]; then
        cp $SOURCE_CRONFILE /etc/cron.daily/
    fi

    # Try to start crond service
    if [ "$SYSTEM_MANAGER" = "INIT" ]; then
        $CRONDINIT status
        if [ $? -eq 0 ]; then
            output "The autobackup function enabled."
            exit 0
        else
            $CRONDINIT start
            if [ $? -eq 0 ]; then
                output "The autobackup function enabled."
                exit 0
            else
                output "Failed to start crond service. Cannot enable autobackup function."
                rm -rf "$CRONFILE"
                exit 0
            fi
        fi
    elif [ "$SYSTEM_MANAGER" = "SYSTEMD" ]; then
        systemctl status crond
        if [ $? -eq 0 ]; then
            output "The autobackup function enabled."
            exit 0
        else
            systemctl start crond
            if [ $? -eq 0 ]; then
                output "The autobackup function enabled."
                exit 0
            else
                output "Failed to start crond service. Cannot enable autobackup function."
                rm -rf "$CRONFILE"
                exit 0
            fi
        fi
    else
        output "Failed to start crond service. Cannot enable autobackup function."
        rm -rf "$CRONFILE"
        exit 0
    fi
}

dodisable() {
    log "Disable autobackup function."

    # Check the crond file
    if [ -f "$CRONFILE" ]; then
        rm -rf $CRONFILE
    fi

    output "The autobackup function disabled."
}

log() {
    local m="$1"
    local date="$(date '+%Y-%m-%d %H:%M:%S')"
    local pid="$$"
    printf "%s\n" "${date} ${pid}: ${m}" >> "${LOG}"
}

output() {
    local m="$1"
    log "${m}"
    printf "%s\n" "${m}"
}

## Main
#Check if the ovirt-engine is available
if [ ! -f "$ENGINE_FILE1" -a ! -f "$ENGINE_FILE2" ]; then
    echo "The ovirt-engine is not available. "
    echo "Please make sure you install and setup the ovirt-engine correctly."
    exit 0
fi

#create one if the log file does not exist
if [ ! -f "$LOG" ]; then
    touch $LOG
fi

# Do this in function so we do not lose $@
parseArgs "$@"

checksystem

do${ACTION}
output "Done."
